<?php 

// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/db.php';
include_once dirname ( __FILE__ ) . '/../components/search.php';


if(isset($_POST['input'])){
	$input = $_POST['input'];
	
	$result = search_pattern ( $input );
	if (sizeof ( $result ) > 0) {
		echo "<ul>";
		foreach ( $result as $row ) {
			echo "<li>";
			echo '<b><a href="?action=show&page=article&id=' . $row ['ID'] . '">' . $row ['Name'] . '</a></b><br/>' . $row ['Content'];
			echo "</li>";
		}
		echo "</ul>";
	} else {
		echo "Geen zoekresultaten gevonden.";
	}
}



?>