<?php 
/**
 * Users.php - renders, well, it should render a list of users
 * 
 * @author Bugslayer
 * 
 */
 
// Check if the request is done by an authorized user. If not, show 401.php and exit
if (!isAuthenticated()) {
	include '401.php';
	exit();
}

global $mysqli;
$sql = $mysqli->query("SELECT userid, Name_first, Name_middle, Name_Last, Email FROM User ORDER BY Name_last ASC");

?>
<h1>Gebruikers</h1>
<p><b>TODO</b> insert users table here</p>

<table>
	<tr>
		<th>Gebruiker ID</th>
		<th>Naam</th>
		<th>Email adres</th>
	</tr>
	<?php 
	foreach ($sql as $sql){
		echo "<tr><td>" . $sql['userid'] . "</td>";
		echo "<td>" . $sql['Name_first'] . " " . $sql['Name_middle'] . " " . $sql['Name_Last'] . "</td>";
		echo "<td> <a href= 'mailto: " . $sql['Email'] .  "'>Email this person </a></tr>";
	}
	
	?>
</table>
